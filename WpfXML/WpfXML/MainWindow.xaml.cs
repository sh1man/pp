﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace WpfXML
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const String filename = "XMLFile1.xml";

        DataSet dataSet = new DataSet();
       


        public MainWindow()
        {
            InitializeComponent();
            //Считывание данных с файла
            dataSet.ReadXml(@"XMLFile1.xml");
        }

        private void ReadXmlButton_Click(object sender, RoutedEventArgs e)
        {
            //Добавление данных в элемент DataGrid
            DataView dataView = new DataView(dataSet.Tables[0]);
            dataGridView1.ItemsSource = dataView;
        }

        private void DataToTexxt(object sender, RoutedEventArgs e)
        {
            //Преобразование xml данных в структуру
            System.IO.StringWriter swXML = new System.IO.StringWriter();
            dataSet.WriteXmlSchema(swXML);
            TextBox1.Text = swXML.ToString();
        }
    }
}
