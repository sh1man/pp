﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TestNagruzki
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
  
        DispatcherTimer timer = new DispatcherTimer();
        Stopwatch sw = new Stopwatch();
        string currentTime = string.Empty;
        private static readonly HttpClient client = new HttpClient();
        string urlAdressHttp { get; set; }
        int countReq = 0;
        public MainWindow()
        {
            InitializeComponent();

            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += dt_Tick;

        }




        void dt_Tick(object sender, EventArgs e)
        {
            if (sw.IsRunning)
            {
                TimeSpan ts = sw.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}",ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

                
                lblTime.Text = currentTime;
            }
        }

        async void testReqAsync()
        {
            if (sw.IsRunning)
            {
                for (int i = 0; countReq > i; i++)
                {
                    var responseString = await client.GetStringAsync(urlAdressHttp);
                }
            }
            sw.Stop();
            Dispatcher.Invoke(() => ListData.Items.Add($"кол-во запросов: {countReq}\nобщее время: {currentTime}\n------------------"));
        }

        private void startbtn_Click(object sender, RoutedEventArgs e)
        {
            sw.Reset();
            lblTime.Text = "00:00:00";
            urlAdressHttp = urlAddres.Text;
            countReq = Int32.Parse(countRequest.Text);
            sw.Start();
            timer.Start();
            Task taskProc = new Task(testReqAsync);
            taskProc.Start();
        }

  
        private void stopbtn_Click(object sender, RoutedEventArgs e)
        {
            if (sw.IsRunning)
            {
                sw.Stop();
                sw.Reset();
                lblTime.Text = "00:00:00";
            }
            ListData.Items.Add($"кол-во запросов: {countReq} время {currentTime}");
        }

        private void resetbtn_Click(object sender, RoutedEventArgs e)
        {
            sw.Reset();
            lblTime.Text = "00:00:00";
        }


    }
}
